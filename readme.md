# Versicherungsberechnung

Anforderungen an die Versicherungsberechnung:
   
    Diese Versicherung ist für Studenten im Alter von 18 bis 25 Jahren. 
	Männer zahlen 140 Euro. Frauen zahlen 15% mehr.
	
Die Anwendung ist eine Spring Boot Anwendung. Baut man die Anwendung und startet sie (Main Methode in Application.java), wird ein Spring Boot Server gestartet und die Anwendung ist auf http://localhost erreichbar.	

	
In den Controllern können der BuggyInsuranceCalculator und der GoodInsuranceCalculator verwendet werden.
	
Enthalten sind UnitTests, Integrationstests und GUI Tests für Firefox. Die Selenium Tests benötigen die laufende Anwendung, um durchgeführt werden zu können.




## Server install
sed -i 103d /home/geh/.ssh/known_hosts

Docker: https://docs.docker.com/engine/install/debian/

sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
	apparmor \
	screen
	
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-compose openjdk-17-jre

sudo docker run hello-world
java -version

scp versicherungsberechnung-0.3.0.jar root@test:~
scp docker-compose.yml root@test:~
scp -r docker-data root@test:~
chmod 777 docker-data/*
promethes.yml anpassen Scrape Target test.host1337.de