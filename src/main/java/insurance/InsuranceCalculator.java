package insurance;

public interface InsuranceCalculator {

	public int calculate(Person person);
}