package insurance;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.togglz.core.manager.FeatureManager;

import insurance.togglz.InsuranceFeatures;

@Controller
public class InsuranceController {

    Counter controllerCalls = Metrics.counter("controllerCalls");

    @Autowired
    private FeatureManager manager;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private InsuranceCalculator goodCalculator = new GoodInsuranceCalculator();
    private InsuranceCalculator buggyCalculator = new BuggyInsuranceCalculator();

    public InsuranceController(FeatureManager manager) {
        this.manager = manager;
    }

    @GetMapping("/")
    public String insuranceForm(Model model) {
        model.addAttribute("person", new Person());
        if (InsuranceFeatures.GOODCALCULATOR.isActive()) {
            return "insuranceGood";
        } else {
            return "insurance";
        }

    }

    @PostMapping("/insurance")
    public String insuranceSubmit(@ModelAttribute Person person) {
        log.debug(person.toString());
        log.debug("GOODCALCULATOR active: " + manager.isActive(InsuranceFeatures.GOODCALCULATOR));

        controllerCalls.increment();

        // Correct results, but the resultpage changed which is bad for simple GUI tests
        if (InsuranceFeatures.CHANGEDRESULTPAGE.isActive()) {
            person.setRate(goodCalculator.calculate(person));
            return "insuranceResultGoodChanged";
        }

        // Correct results
        if (InsuranceFeatures.GOODCALCULATOR.isActive()) {
            person.setRate(goodCalculator.calculate(person));
            return "insuranceResultGood";
        }



        // Incorrect results
        if (!InsuranceFeatures.GOODCALCULATOR.isActive()) {
            person.setRate(buggyCalculator.calculate(person));
            return "insuranceResult";
        }

        return "";
    }

}
