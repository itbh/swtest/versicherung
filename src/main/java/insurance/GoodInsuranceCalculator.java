package insurance;

import io.micrometer.core.instrument.Metrics;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GoodInsuranceCalculator implements InsuranceCalculator {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public int calculate(Person person) {
		int result = -1;

		calculateAge(person);

		if (person.getStudent().equals("j")) {
			if (person.getAge() >= 18 && person.getAge() <= 25) {
				if (person.getGender().equals("m")) {
					result = 140;
				} else {
					result = 161;
				}
			}
		}

		log.info("Versicherungsberechnung durchgeführt. Ergebnis: {}", result);
		return result;

	}

	private void calculateAge(Person person)  {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
		try {
			DateTime date = formatter.parseDateTime(person.getDob());
			Period p = new Period(date, new DateTime());
			person.setAge(p.getYears());
		} catch (Exception e) {
			log.error("Geburtsdatum fehlerhaft");
		}
	}

}
