package insurance;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class BuggyInsuranceCalculator implements InsuranceCalculator {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public int calculate(Person person) {
		int result = -1;

		calculateAge(person);

		insertSomeBugs(person);

		if (person.getStudent().equals("j")) {
			if (person.getAge() >= 18 && person.getAge() <= 25) {
				result = 140;
			}
			if ((person.getAge() > 18 && person.getAge() < 25) && person.getGender().equals("w")) {
				result = 162;
			}
		}

		return result;
	}

	private void calculateAge(Person person) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
		try {
			DateTime date = formatter.parseDateTime(person.getDob());
			Period p = new Period(date, new DateTime());
			person.setAge(p.getYears());
		} catch (Exception e) {
			log.error("Geburtsdatum fehlerhaft: ",person.getDob());
		}
	}

	private void insertSomeBugs(Person person) {
		String newName = person.getName().replaceAll("[ä|ö|ü|ß]", "□");
		person.setName(newName);

		if (person.getStudent().isEmpty() || person.getGender().isEmpty()) {
			throw new NullPointerException("NullPointerException: Unexpected Value of Null");
		}

		// A performance issue here
		long b = new Random().nextInt(10) * 1000;
		try {
			Thread.sleep(b);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public void doSomethingCrappy(Person i) {

		i.getName();

		int a;

		a = 5;
		a = 10;

		if (a == 10) {
			return;
		} else {
			return;
		}

	}

}
