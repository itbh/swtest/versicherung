package insurance;

public class Person {

	private String name;

	private String gender;

	private int age;

	private String student;

	private String dob = "TT.MM.JJJJ";

	private int rate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getStudent() {
		return student;
	}

	public void setStudent(String student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "Insurance [name=" + name + ", gender=" + gender + ", age=" + age + ", student=" + student + ", dob=" + dob
				+ ", rate=" + rate + "]";
	}
}
