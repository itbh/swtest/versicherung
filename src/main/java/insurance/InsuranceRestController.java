package insurance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import insurance.togglz.InsuranceFeatures;

@RestController
public class InsuranceRestController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private InsuranceCalculator goodCalculator = new GoodInsuranceCalculator();
	private InsuranceCalculator buggyCalculator = new BuggyInsuranceCalculator();

	@RequestMapping("rest")
	public Person calculate(@RequestParam(value = "name", defaultValue = "") String name,
			@RequestParam(value = "gender", defaultValue = "m") String gender,
			@RequestParam(value = "student", defaultValue = "n") String student,
			@RequestParam(value = "dob") String dob) {

		Person i = new Person();
		i.setName(name);
		i.setGender(gender);
		i.setStudent(student);
		i.setDob(dob);

		log.debug("Restcall: " + i.toString());

		if (InsuranceFeatures.GOODCALCULATOR.isActive()) {
			i.setRate(goodCalculator.calculate(i));
		}

		if (!InsuranceFeatures.GOODCALCULATOR.isActive()) {
			i.setRate(buggyCalculator.calculate(i));
		}

		return i;
	}
}
