package insurance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GoodInsuranceCalculatorTest {

	@Test
	public void testMaleStudent18() {
		
		// Arrange
		Person person = new Person();
		person.setStudent("j");
		person.setDob("01.01.2001");
		person.setGender("m");
		
		// Act
		InsuranceCalculator calculator = new GoodInsuranceCalculator();
		int rate = calculator.calculate(person);
		
		// Assert
		assertEquals(140, rate);
	}
	
	@Test
	public void testMaleStudent17() {

		// Arrange
		Person person = new Person();
		person.setStudent("j");
		person.setDob("01.01.2006");
		person.setGender("m");
		
		// Act
		InsuranceCalculator calculator = new GoodInsuranceCalculator();
		int rate = calculator.calculate(person);
		
		// Assert
		assertEquals(-1, rate);
	}

	@Test
	public void testMaleStudent27() {

		// Arrange
		Person person = new Person();
		person.setStudent("j");
		person.setDob("01.01.1992");
		person.setGender("m");
		
		// Act
		InsuranceCalculator calculator = new GoodInsuranceCalculator();
		int rate = calculator.calculate(person);
		
		// Assert
		assertEquals(-1, rate);
	}

	@Test
	public void testNoStudent27() {

		// Arrange
		Person person = new Person();
		person.setStudent("n");
		person.setDob("01.01.1992");
		person.setGender("m");
		
		// Act
		InsuranceCalculator calculator = new GoodInsuranceCalculator();
		int rate = calculator.calculate(person);
		
		// Assert
		assertEquals(-1, rate);
	}

	@Test
	public void testFemaleStudent25() {

		// Arrange
		Person person = new Person();
		person.setStudent("j");
		person.setDob("01.01.1998");
		person.setGender("w");
		
		// Act
		InsuranceCalculator calculator = new GoodInsuranceCalculator();
		int rate = calculator.calculate(person);
		
		// Assert
		assertEquals(161, rate);
	}
	
//	@Test(expected = RuntimeException.class)
//	public void testWrongDOB() {
//		// Arrange
//		Person person = new Person();
//		person.setStudent("j");
//		person.setDob("x");
//		person.setGender("w");
//
//		// Act
//		InsuranceCalculator calculator = new GoodInsuranceCalculator();
//		calculator.calculate(person);
//
//	}

}
