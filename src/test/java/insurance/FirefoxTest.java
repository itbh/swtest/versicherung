package insurance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@Disabled
public class FirefoxTest {

	
	private static WebDriver driver;
	private static WebDriverWait wait;
	private static String url;

	@Disabled
	@BeforeAll
	public static void setUp() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,Duration.ofSeconds(5));
		url = "http://localhost/";
	}
	
	@AfterAll
	@Disabled
	public static void tearDown() {
		
		driver.quit();
	}
	
	@Disabled
	@Test
	public void testStudentM25() {
		
		driver.get(url);
		assertEquals("Versicherungsberechnung", driver.getTitle());
		
		driver.findElement(By.id("name")).sendKeys("Daniel");
		driver.findElement(By.id("gender")).sendKeys("m");
		driver.findElement(By.id("dob")).clear();
		driver.findElement(By.id("dob")).sendKeys("01.01.1995");
		driver.findElement(By.id("student")).sendKeys("j");
		driver.findElement(By.id("student")).submit();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/p[5]")));
		
		assertEquals("Rate: 140 Euro",driver.findElement(By.xpath("/html/body/p[5]")).getText());
		
	}

	@Test
	@Disabled
	public void testStudentW25() {
		
		driver.get(url);
		assertEquals("Versicherungsberechnung", driver.getTitle());
		
		driver.findElement(By.id("name")).sendKeys("Daniela");
		driver.findElement(By.id("gender")).sendKeys("w");
		driver.findElement(By.id("dob")).clear();
		driver.findElement(By.id("dob")).sendKeys("01.01.1995");
		driver.findElement(By.id("student")).sendKeys("j");
		driver.findElement(By.id("student")).submit();

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/p[5]")));
		
		assertEquals("Rate: 161 Euro",driver.findElement(By.xpath("/html/body/p[5]")).getText());
		
	}

	
}
